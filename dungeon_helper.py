#!/usr/bin/env python3
#
# UMBC TDQC5
# Bruce Cain

import random


class Room:
    """
        Room(self, description: str, total_monsters: int,
             item_list: list(), monster_list: list()) -> Room

        Room class builds a room that the hero is currently in.
        description is a string that describes the room.
        total_monsters is an integer that determines how many monsters
            in the current room.
        item_list stores all the possible lists that can spawn.
        monster_list is the list of all possible monsters.
    """
    def __init__(self, description, total_monsters,
                 item_list, monster_list):
        self.description = gen_room_description()
        self.total_monsters = total_monsters
        self.items = gen_items(total_monsters, item_list)
        self.dropped_items = list()
        self.monsters = gen_monsters(total_monsters, monster_list)


class Sprite:
    """
        Sprite(self, hp: int, name: string, total_rolls: int) -> Sprite

        Sprite class has the common attributes of severl characters
            in the dungeon.
        hp is the total hp the sprite starts with.
        name is the name of the sprite.
        total_rolls is the maximum amount of rolls a sprite can have.
    """
    def __init__(self, hp=0, name=None, total_rolls=0):
        self.hp = hp
        self.max_hp = hp
        self.name = name
        self.total_rolls = total_rolls


class Hero(Sprite):
    """
        Hero(self) -> Hero

        Hero class inherits from the Sprite class,
            the hero is the player in question.
    """
    def __init__(self):
        super().__init__(10, "Hero", 3)
        self.monsters_defeated = 0
        self.initiative = 0
        self.items = dict()


class Monster(Sprite):
    """
        Monster(self, name: str, hp: int, total_rolls: int) -> Monster

        Monster class inherits from the Sprite class, it builds a monster for
            the room.
        name is the name of the monster.
        hp is the total hp of said monster.
        total_rolls is the maximum number of rolls the monster has.
    """
    def __init__(self, name=None, hp=0, total_rolls=0):
        super().__init__(hp, name, total_rolls)
        self.dice_rolled = None


def gen_room_description():
    """
        gen_room_description() -> str

        Builds a string the describes a room.
    """
    land = ["Forest", "Cave", "Plains", "Mountains"]
    adjetive = ["Dark", "Damp", "Blue", "Purple"]

    room = "You enter the {} {}." \
           .format(random.choice(adjetive), random.choice(land))

    return room


def gen_monsters(total_monsters, mobs):
    """
        gen_monsters(total_monsters: int, mobs: list()) -> list()

        Generates a list of random monsters for a room.
        total_monsters is the maximum number of monster to add to the list
            to spawn in a room.
        mobs is the list of all possible monsters.
    """
    if total_monsters == 0:
        return None

    mons = []
    for i in range(0, total_monsters):
        mob = random.choice(mobs[1:]).split()
        try:
            mons.append(Monster(mob[0], int(mob[1]), int(mob[2])))
        except ValueError:
            print("Bad monster hp or number of rolls in monster file\n")

    return mons


def gen_items(total_items, items):
    """
        gen_items(total_items: int, items: list()) -> list()

        Generates a list of random items that could possible be dropped
            into a room.
        total_items is the maximum number of items to be able to be
            dropped into the room.
        items is the list of possible items that can be dropped.
    """
    room_items = []
    for i in range(0, total_items):
        room_items.append(random.choice(items)[:-1])

    return room_items


def battle_menu(room, hero, show_dice):
    """
        battle_menu(room: Room, hero: Hero, show_dice: int) -> int

        Logic for the battle menu when in a battle.
        room is a Room that the hero is currently in.
        hero is the player.
        show_dice is an option if the user whats to see what is
            rolled during the combat phase.
    """
    monster = room.monsters[0]

    print("Hero's Health: {}\n"
          "Monster's Name: {}\n"
          "Monster's Health {}\n"
          "---------------------\n"
          "1. Attack\n"
          "2. Run"
          .format(hero.hp, monster.name, monster.hp))

    if "Attack Potion" in hero.items:
        print("3. Use attack potion.")

    selection = 0

    while selection == 0:
        try:
            selection = input("> ")
        except KeyboardInterrupt:
            quit(hero)
            selection = 0
            continue

        try:
            selection = int(selection)
        except ValueError:
            selection = 0

        if selection == 1:
            battle(hero, monster, show_dice)

        elif selection == 2:
            if random.random() < (hero.hp * .1):
                print("Ran away successfully.")
                return 1
            else:
                hero.total_rolls = 1
                battle(hero, monster, show_dice)

        elif "Attack Potion" in hero.items and selection == 2:
            print("Attack Potion used.")
            hero.total_rolls += 1
            del(hero.items["Attack Potion"])

        else:
            print("Invalid selection.")

    return 0


def nonbattle_menu(room, hero):
    """
        nonbattle_menu(room: Room, hero: Hero) -> int

        Provides interfaces to the menu outside of battle phase.
        room is where the hero is currently.
        hero is the player.
    """
    print("Hero's Health: {}".format(hero.hp))

    if len(room.dropped_items) != 0:
        for item in room.dropped_items:
            print(" {}".format(item))

    print("Actions\n------------\n"
          "1. Go to next room\n"
          "2. Get loot\n"
          "3. Show loot\n"
          "4. Quit")

    selection = 0

    while selection == 0:
        try:
            selection = input("> ")
        except KeyboardInterrupt:
            quit(hero)
            continue

        try:
            selection = int(selection)
        except ValueError:
            selection = 0

        if selection == 1:
            if room.total_monsters != 0:
                print("There are still monsters alive, FINISH THEM!\n")
            else:
                return 1

        elif selection == 2:
            if len(room.dropped_items) == 0:
                print("No dropped loot")
                selection = 0
                continue

            for loot in room.dropped_items:
                if loot in hero.items:
                    hero.items[loot] += 1
                else:
                    hero.items[loot] = 1

        elif selection == 3:
            if len(hero.items) == 0:
                print("No items being carried.")
                selection = 0
                continue

            print("Hero's Items\n--------------")
            for item in hero.items:
                print(" {}".format(item))

        elif selection == 4:
            quit(hero)

        else:
            print("Invalid selection.\n")

    return 0


def battle(hero, monster, show_dice):
    """
        battle(hero: Hero, monster: Monster, show_dice: int) -> None

        Runs a battle between the hero and current monster.
        hero is the player.
        monster is the current monster being battled.
        show_dice will show the rolled dice if set to 1
    """
    hero_rolls = roll_dice(hero.total_rolls)
    monster.dice_rolled = roll_dice(monster.total_rolls)
    outcome = 0

    if show_dice == 1:
        print("Hero rolled: {}\nMonster rolled: {}"
              .format(" ".join(str(num) for num in hero_rolls),
                      " ".join(str(num) for num in monster.dice_rolled)))

    if hero.total_rolls > 3 or hero.total_rolls == 1:
        hero.total_rolls = 3

    if max(hero_rolls) > max(monster.dice_rolled):
        monster.hp -= 1
    elif max(hero_rolls) < max(monster.dice_rolled):
        outcome = 1
        hero.hp -= 1
    else:
        if hero.initiative == 1:
            monster.hp -= 1
        else:
            outcome = 1
            hero.hp -= 1

    if outcome == 1:
        print("{} deals 1 damage to Hero\n".format(monster.name))
    else:
        print("Hero deals 1 damage to {}\n".format(monster.name))


def check_health(hero, room):
    """
        check_health(hero: Hero, room: Room) -> int

        Checks the health of monster and hero.
        hero is the player.
        room is the current room the hero is in.
    """
    if room.total_monsters != 0 and room.monsters[0].hp == 0:
        print("Hero defeated {}!\n".format(room.monsters[0].name))
        dropped_loot(hero, room)
        hero.monsters_defeated += 1
        room.monsters.pop(0)
        room.total_monsters -= 1
        return 1

    if hero.hp == 0:
        credits(hero)
        exit(1)

    return 0


def dropped_loot(hero, room):
    """
        dropped_loot(hero: Hero, room: Room) -> None

        Determines if any loot is dropped.
        hero is the player.
        room is the current room the hero is in.
    """
    chance = (max(room.monsters[0].dice_rolled) +
              room.monsters[0].max_hp) * .1

    if random.random() < chance:
        item = room.items.pop()
        print("{} dropped\n".format(item))
        room.dropped_items.append(item)


def initiative(hero, monster, contact, show_dice):
    """
        initiative(hero: Hero, monster: Monster,
                   contact: int, show_dice: int) -> None

        Determines who has initiative.
        hero is the player.
        monster is the current monster being fought.
        contact determines if this is the first contact or not.
        show_dice will show dice if set to 1
    """
    if contact == 0:
        return

    if hero.initiative == 0 and contact == 1:
        print("Monsters has initiative!")
        battle(hero, monster, show_dice)


def credits(hero):
    """
        credit(hero: Hero) -> None

        Prints the ending credits(You never win this game)
        hero is the player
    """
    print("YOU LOSE")
    print("Monsters defeated {}".format(hero.monsters_defeated))

    print("Loot:")
    for item, total in hero.items.items():
        print(" {}: {}".format(item, total))


def roll_dice(tot_rolls):
    """
        roll_dice(tot_rolls: int) -> list()

        Builds a list of total want rolls of a 6 sided dice.
        tot_rolls is the total number of rolls to be rolled.
    """
    rolls = []

    for i in range(0, tot_rolls):
        rolls.append(random.randint(1, 6))

    return rolls


def quit(hero):
    print("\nAre you sure you want to quit?(y/n)")

    answer = 0
    while answer == 0:
        answer = input("> ")

        if answer.lower() in ["y", "yes"]:
            credits(hero)
            exit(1)
        elif answer.lower() in ["n", "no"]:
            answer = 1
        else:
            print("Enter y/yes or n/no")
            answer = 0


def clear_screen():
    """
        clear_screen() -> None

        Clears the screen.
    """
    try:
        input("Enter to continue...")
    except KeyboardInterrupt:
        pass

    print("\033[2J\033[H")
