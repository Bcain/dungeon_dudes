#!/usr/bin/env python3
#
#
#
#
# UMBC TDQC5
# Bruce Cain

import random
import sys
import dungeon_helper


def main():
    """
        The driver function.
    """
    playing = 1
    show_dice = 0
    hero = dungeon_helper.Hero()

    if "-d" in sys.argv:
        show_dice = 1

    elif len(sys.argv) >= 2:
        print("Invalid command line arguments.")
        return -1

    items = []
    with open(".dd_loot") as f:
        for line in f:
            items.append(line)

    mobs = []
    with open(".dd_monsters") as f:
        for line in f:
            mobs.append(line)

    while playing == 1:
        room = dungeon_helper.Room("Room", random.randint(1, 3), items, mobs)
        first_contact = 1

        print("{}\n{} monsters\n"
              .format(room.description, room.total_monsters))

        hero_roll = dungeon_helper.roll_dice(1)
        monster_roll = dungeon_helper.roll_dice(1)
        if max(monster_roll) < max(hero_roll):
            hero.initiative = 1

        move = 0
        while move == 0:

            if room.total_monsters != 0:
                dungeon_helper.initiative(hero,
                                          room.monsters[0],
                                          first_contact,
                                          show_dice)

            first_contact = dungeon_helper.check_health(hero, room)

            if first_contact == 0 and room.total_monsters != 0:
                move = dungeon_helper.battle_menu(room, hero, show_dice)
            elif room.total_monsters == 0:
                move = dungeon_helper.nonbattle_menu(room, hero)

            dungeon_helper.clear_screen()

if __name__ == "__main__":
    main()
