#!/usr/bin/env python3
#
# UMBC TDQC5
# Bruce Cain

import unittest
from mywork.dungeon_dudes.dungeon_helper import Sprite
from mywork.dungeon_dudes.dungeon_helper import Hero
from mywork.dungeon_dudes.dungeon_helper import Monster
from mywork.dungeon_dudes.dungeon_helper import Room


class DungeonDudesTest(unittest.TestCase):

    def test_sprite_init(self):
        sprites = Sprite(10, "Hero")
        self.assertEqual(sprites.hp, 10)
        self.assertEqual(sprites.max_hp, 10)
        self.assertEqual(sprites.name, "Hero")

    def test_hero_init(self):
        hero = Hero()
        self.assertEqual(hero.hp, 10)
        self.assertEqual(hero.name, "Hero")
        self.assertEqual(hero.items, None)

    def test_monster_init(self):
        monster = Monster()
        self.assertEqual(monster.hp, 0)
        self.assertEqual(monster.name, None)
        self.assertEqual(monster.total_rolls, 0)
        
    def test_room_init(self):
        room = Room("Room", 1, None)
        self.assertEqual(room.description, "Room")
        self.assertEqual(room.total_monsters, 1)
        self.assertEqual(room.items, None)

if __name__ == "__main__":
    unittest.main(verbosity=2)

